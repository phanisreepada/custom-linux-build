#!/bin/bash
echo "Kernel Headers"
echo "read this message carefully"
echo " Uncompress the kernel tarball and change into its directory. Then run:"

echo " for example tar -jxvf linux-4.16.xx    then cd linux-4.16.xx/"

echo "complete the above procedure to avoid error free compilation"

make mrproper
make ARCH=${CLOS_ARCH} headers_check && \
make ARCH=${CLOS_ARCH} INSTALL_HDR_PATH=dest headers_install
cp -rv dest/include/* ${CLOS}/usr/include

echo "Binutils"
echo "Binutils contains a linker, assembler and other tools needed to handle compiled object files"
echo " Uncompress the binutils tarball. Then create the binutils-build directory and change into it: otherwies error "


mkdir binutils-build
cd binutils-build/

echo "Then now running:"


../binutils-2.30/configure --prefix=${CLOS}/cross-tools \
--target=${CLOS_TARGET} --with-sysroot=${CLOS} \
--disable-nls --enable-shared --disable-multilib
make configure-host && make
ln -sv lib ${CLOS}/cross-tools/lib64
make install

echo "Coping  over the following header file to the target's filesystem:"


cp -v ../binutils-2.30/include/libiberty.h ${CLOS}/usr/include

echo "GCC (Static)"

echo "Before building the final cross-compiler toolchain, you first must build a statically compiled toolchain to build the C library (glibc) to which the final GCC cross compiler will link."


echo "Uncompress the GCC tarball, and then uncompress the following packages and move them into the GCC root directory:"


tar xjf gmp-6.1.2.tar.bz2
mv gmp-6.1.2 gcc-7.3.0/gmp
tar xJf mpfr-4.0.1.tar.xz
mv mpfr-4.0.1 gcc-7.3.0/mpfr
tar xzf mpc-1.1.0.tar.gz
mv mpc-1.1.0 gcc-7.3.0/mpc


echo "Now create a gcc-static directory and change into it:"


mkdir gcc-static
cd gcc-static/

echo "Running the following commands:"


AR=ar LDFLAGS="-Wl,-rpath,${CLOS}/cross-tools/lib" \
../gcc-7.3.0/configure --prefix=${CLOS}/cross-tools \
--build=${CLOS_HOST} --host=${CLOS_HOST} \
--target=${CLOS_TARGET} \
--with-sysroot=${CLOS}/target --disable-nls \
--disable-shared \
--with-mpfr-include=$(pwd)/../gcc-7.3.0/mpfr/src \
--with-mpfr-lib=$(pwd)/mpfr/src/.libs \
--without-headers --with-newlib --disable-decimal-float \
--disable-libgomp --disable-libmudflap --disable-libssp \
--disable-threads --enable-languages=c,c++ \
--disable-multilib --with-arch=${CLOS_CPU}
make all-gcc all-target-libgcc && \
make install-gcc install-target-libgcc
ln -vs libgcc.a `${CLOS_TARGET}-gcc -print-libgcc-file-name | sed 's/libgcc/&_eh/'`

echo "Do not delete these directories; you'll need to come back to them from the final version of GCC."

echo "Glibc"

echo "Uncompress the glibc tarball. Then create the glibc-build directory and change into it:"


mkdir glibc-build
cd glibc-build/


echo "Configure the following build flags:"


echo "libc_cv_forced_unwind=yes" > config.cache
echo "libc_cv_c_cleanup=yes" >> config.cache
echo "libc_cv_ssp=no" >> config.cache
echo "libc_cv_ssp_strong=no" >> config.cache

echo " running the following :"


BUILD_CC="gcc" CC="${CLOS_TARGET}-gcc" \
AR="${CLOS_TARGET}-ar" \
RANLIB="${CLOS_TARGET}-ranlib" CFLAGS="-O2" \
../glibc-2.27/configure --prefix=/usr \
--host=${CLOS_TARGET} --build=${CLOS_HOST} \
--disable-profile --enable-add-ons --with-tls \
--enable-kernel=2.6.32 --with-__thread \
--with-binutils=${CLOS}/cross-tools/bin \
--with-headers=${CLOS}/usr/include \
--cache-file=config.cache
make && make install_root=${CLOS}/ install

echo "GCC (Final)"

echo "you'll now build the final GCC cross compiler that will link to the C library built and installed in the previous step. Create the gcc-build directory and change into it:"


mkdir gcc-build
cd gcc-build/

echo "Now running :"


AR=ar LDFLAGS="-Wl,-rpath,${CLOS}/cross-tools/lib" \
../gcc-7.3.0/configure --prefix=${CLOS}/cross-tools \
--build=${CLOS_HOST} --target=${CLOS_TARGET} \
--host=${CLOS_HOST} --with-sysroot=${CLOS} \
--disable-nls --enable-shared \
--enable-languages=c,c++ --enable-c99 \
--enable-long-long \
--with-mpfr-include=$(pwd)/../gcc-7.3.0/mpfr/src \
--with-mpfr-lib=$(pwd)/mpfr/src/.libs \
--disable-multilib --with-arch=${CLOS_CPU}
make && make install
cp -v ${CLOS}/cross-tools/${CLOS_TARGET}/lib64/libgcc_s.so.1 ${CLOS}/lib64

echo "export PATH"
export CC="${CLOS_TARGET}-gcc"
export CXX="${CLOS_TARGET}-g++"
export CPP="${CLOS_TARGET}-gcc -E"
export AR="${CLOS_TARGET}-ar"
export AS="${CLOS_TARGET}-as"
export LD="${CLOS_TARGET}-ld"
export RANLIB="${CLOS_TARGET}-ranlib"
export READELF="${CLOS_TARGET}-readelf"
export STRIP="${CLOS_TARGET}-strip"
