#!/bin/bash
echo "Installing the Target Image precedure "
echo "All of the cross compilation is complete. Now you have everything you need to install the entire cross-compiled operating system to either a physical or virtual drive, but before doing that, let's not tamper with the original target build directory by making a copy of it:"


cp -rf ${CLOS}/ ${CLOS}-copy

echo "Use this copy for the remainder of this tutorial. Remove some of the now unneeded directories:"


rm -rfv ${CLOS}-copy/cross-tools
rm -rfv ${CLOS}-copy/usr/src/*

echo "Followed by the now unneeded statically compiled library files (if any):"


FILES="$(ls ${CLOS}-copy/usr/lib64/*.a)"
for file in $FILES; do
> rm -f $file
> done

echo "Now strip all debug symbols from the installed binaries. This will reduce overall file sizes and keep the target image's overall footprint to a minimum:"

echo " you can omit warnings generated after this step"
find ${CLOS}-copy/{,usr/}{bin,lib,sbin} -type f -exec sudo strip --strip-debug '{}' ';'
find ${CLOS}-copy/{,usr/}lib64 -type f -exec sudo strip --strip-debug '{}' ';'

echo "Finally, change file ownerships and create the following nodes:"


sudo chown -R root:root ${CLOS}-copy
sudo chgrp 13 ${CLOS}-copy/var/run/utmp ${CLOS}-copy/var/log/lastlog
sudo mknod -m 0666 ${CLOS}-copy/dev/null c 1 3
sudo mknod -m 0600 ${CLOS}-copy/dev/console c 5 1
sudo chmod 4755 ${CLOS}-copy/bin/busybox

echo "Change into the target copy directory to create a tarball of the entire operating system image:"


cd {CLOS}-copy/
sudo tar cfJ ../clos-build-26April2021.tar.xz *

echo "Notice how the target image is less than 60MB. You built that—a minimal Linux operating system that occupies less than 60MB of disk space:"


sudo du -h|tail -n1

echo "58M  ??!!!! or less "

echo "OS compressed "


ls -lh clos-build-26April2021.tar.xz
echo " see somthing like this -rw-r--r-- 1 root root 18M Apr 26 15:31 clos-build-26April2021.tar.xz "

